// import { BrowserModule } from '@angular/platform-browser';
// import { NgModule } from '@angular/core';
//
// import { AppRoutingModule } from './app-routing.module';
//
// //Components
// import { AppComponent } from './app.component';
// import {UserComponent} from "./components/user/user.component";
// import {HelloComponent} from "./components/hello/hello.component";
// import {TravelComponent} from "./components/travel/travel.component";
// import { CodeboundComponent } from './components/codebound/codebound.component';
// import { MathComponent } from './components/math/math.component';
// import { VipComponent } from './components/vip/vip.component';
// import { UsersComponent } from './components/users/users.component';

// @NgModule({
//   declarations: [
//     AppComponent,
//     UserComponent,
//     HelloComponent,
//     TravelComponent,
//     CodeboundComponent,
//     MathComponent,
//     VipComponent,
//     UsersComponent
//   ],
//   imports: [ //modules
//     BrowserModule,
//     AppRoutingModule
//   ],
//   providers: [],//services
//   bootstrap: [AppComponent]
// })
// export class AppModule { }

// import {Component} from "@angular/core";
// import { BrowserModule } from '@angular/platform-browser';
// import { NgModule } from '@angular/core';
// import {FormsModule} from '@angular/forms';
// import {AppComponent} from './app.component';
// @NgModule({
//   declarations:[ //components go
//     AppComponent
//   ],
//   imports: [ //modules go here
//     //other imports...
//     BrowserModule,
//     FormsModule
//   ],
//   providers:[],// services go here
//   bootstrap: [
//     AppComponent
//   ]
//
// })
// export class AppModule {
// }


//Reactive Form example
//import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from "./app-routing.module";
import {ParticlesModule} from 'angular-particle';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';




import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
import {VipComponent} from "./components/vip/vip.component";
import {FormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule, HttpHeaders} from "@angular/common/http";
import {MatButtonModule} from '@angular/material/button';

//Services
import {DataService} from "./services/data.service";
import {PostsService} from "./services/posts.service";
import { PostsComponent } from './components/posts/posts.component';
import { PostFormComponent } from './components/post-form/post-form.component';
import { HomeComponent } from './components/home/home.component';
import {RouterModule} from "@angular/router";
import { NavbarComponent } from './components/navbar/navbar.component';
import { TravelComponent } from "./components/travel/travel.component";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialStylingComponent } from './components/material-styling/material-styling.component';


@NgModule({
  declarations: [// components go
    AppComponent, UsersComponent, VipComponent, PostsComponent, PostFormComponent, HomeComponent, NavbarComponent, TravelComponent, MaterialStylingComponent
  ],
  imports: [ // modules go here
    // other imports ...
    //ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ParticlesModule,
    RouterModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule
  ],
  providers: [
    DataService,
    PostsService,
  ], // services go here
  bootstrap: [AppComponent]
})
export class AppModule { }
