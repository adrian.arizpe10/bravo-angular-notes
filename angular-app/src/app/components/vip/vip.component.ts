import { Component, OnInit } from '@angular/core';
import {Member} from "../../models/Member";

@Component({
  selector: 'app-vip',
  templateUrl: './vip.component.html',
  styleUrls: ['./vip.component.css']
})
export class VipComponent implements OnInit {

  member: Member[];
  message: any;
  displayInfo: boolean = false;
  loadingMembers: boolean = false;

  constructor() {

  }

  // startTime() {
  //   this.message = setTimeout(() => {
  //     alert("Rendering VIP members list")
  //   }, 5000);
  // }
  //
  // stopTime() {
  //   this.displayInfo = true;
  //   clearInterval(this.message);
  // }

  ngOnInit(): void {

    setTimeout(() => {this.loadingMembers = true}, 5000)
    this.message = 'Rendering VIP members list'

    this.member = [
      {
        firstName: 'Adrian',
        lastName: 'Arizpe',
        username: 'CodeBound',
        memberNo: 12345
      },
      {
        firstName: 'Henry',
        lastName: 'Bravo',
        username: 'h3',
        memberNo: 54323
      },
      {
        firstName: 'Eric',
        lastName: 'Victor',
        username: 'EV',
        memberNo: 65432
      },
      {
        firstName: 'Jonathan',
        lastName: 'G',
        username: 'Jonathan',
        memberNo: 78909
      },
      {
        firstName: 'MaryAnn',
        lastName: 'Frias',
        username: 'Mary',
        memberNo: 98762
      }
    ];

    //   setTimeout(() => {
  //     this.displayInfo === false
  //   }, 5000)
  }//End of NgOnInit - do not delete
}//End of class
