import { Component, OnInit, ViewChild } from '@angular/core';
import {User} from "../../models/User";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  user: User = {
    firstName: '',
    lastName: '',
    age: null,
    balance: 0,
    email: '',
    hide: false,
    image: '',
    isActive: false,
    //memberSince: undefined,
    showUserForm: false,
  }
  //Properties
  users: User[]; // [] makes users into an array of names
  displayInfo: boolean = true;
  showUserForm: boolean = false;
  enableAddUser: boolean;
  currentClass: {}; //Basically an empty object
  currentStyle:{};
  @ViewChild('userForm')form: any;

  //create a property for the observable
  data: any;


//Inject the service as a dependency in our constructor()
  constructor(private dataService: DataService) { } //private makes 'dataService' usable only in this class - component

  ngOnInit(): void {


    // this.users = this.dataService.getUsers();
    //subscribe to the observable to use getUsers() method
    this.dataService.getUsers().subscribe(users => {
      this.users = users;
    });

    //subscribe to the observable
    // this.dataService.getData().subscribe(data => {
    //   console.log(data)
    // });

    this.enableAddUser = false;
    this.setCurrentClasses();
    this.setCurrentStyle();

    //calling our addUser method
    // this.addUser({
    //   firstName: 'Jason',
    //   lastName: 'Todd',
    //   age: 13
    // });

  }//End of ngOnInit() - do not delete

  //Create a method that adds a new user to the array


  //Create a method that will change the state of our button
  setCurrentClasses() {
    this.currentClass = {
      "btn-dark": this.enableAddUser
    }
  }

  //Create a method that will add padding to the users name
  //when the users information is not displaying (this.displayInfo - false)

  setCurrentStyle() {
    this.currentStyle = {
      'padding-top': this.displayInfo ? '0' : '80px'
    }
  }

  //Adding in fire event method under click event
  //Type of events: mousedown, mouseup, mouseout, click, dblclick
  // fireEvent(e){
    // console.log('The button has been clicked');
    // console.log(e.type);
    // console.log("The drag event has occurred");
  // }

  toggleHide(user:User) {
    user.hide = !user.hide;
  }

  onSubmit({value,valid} : {value:User, valid:boolean}) {
    if (!valid) {
      alert('This form is invalid')
    } else {
      value.isActive = true;
      value.memberSince = new Date();
      value.hide = true;
    }
    // faiths code from forms
    // this.users.unshift(value);

    //Stephens code from services
    this.dataService.addUser(value);
    //Resets form
    this.form.reset();
    //}
  }

  fireEvent(e) {
    console.log('The event has happened');
  }

} //End of class - do not delete
