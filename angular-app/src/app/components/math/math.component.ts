import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {
  //Properties
  num1: number;
  num2: number;

  constructor() {
    // this.num1 = 20;
    // this.num2 = 10;


    this.sum(12, 5);
    this.difference(12, 5);
    this.product(12, 5);
    this.division(12, 5);
    // this.table();
  }

  sum(x, y) { //add parameters
    this.num1 = x;//passing parameter
    this.num2 = y;//passing parameter

    return (this.num1 + this.num2); //not needed
  }
  difference(x, y){
    this.num1 = x;//passing parameter
    this.num2 = y;//passing parameter

    return(this.num1 - this.num2); //not needed
  }
  product(x, y){
    this.num1 = x;//passing parameter
    this.num2 = y;//passing parameter

    return (this.num1 * this.num2); //not needed
  }
  division(x, y){
    this.num1 = x;//passing parameter
    this.num2 = y;//passing parameter

    return (this.num1 / this.num2) //not needed
  }

  table(){
    for (var i=1;i<100;i+=1) {
      if (i % 3 == 0 && i % 15 != 0) {
        console.log('Fizz');
      } else if (i % 5 == 0 && i % 15 !=0) {
        console.log('Buzz');
      } else if ( i % 15 == 0) {
        console.log('FizzBuzz');
      } else {
        console.log(i);
      }

    }
  }

  ngOnInit(): void {
  }

}
