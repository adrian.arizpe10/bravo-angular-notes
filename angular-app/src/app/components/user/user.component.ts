import {Component} from "@angular/core";
import {User} from "../../models/User";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent {
  //Properties - like an attribute of the component
  // firstName = 'Clark';
  // lastName = 'Kent';
  // age = 65;

  //More Practical way of creating properties
  //Syntax: propertyName: datatype

  // firstName: string;
  // lastName: string;
  // age: number;
  // whatever: any;
  // hasKids: boolean;

  //Data-types
  // numberArray: number[]; //this Must be an array of numbers
  // stringArray: string []; //this Must be an array of strings
  // mixArray: any[]; //this can be an array of anything - but it HAS to be an array
  //
  //Might see this more often
  user: User;

  //Methods - a "function" inside of a class
  //-constructor - runs every time when our component is initialized / called
  constructor() {
    // console.log('Hello from UserComponent');
    //
    // this.greeting();
    // console.log(this.age); //undefined
    // this.hasBirthday();
    // console.log(this.age);//31

    // this.firstName = 'Clark';
    // this.lastName = 'Kent';
    // this.age = 65;
    // this.hasKids = false;
    // this.greeting();
    //
    // this.numberArray = [12, 33, 24];
    // this.stringArray = ['12', '34', '24'];
    // this.mixArray = [12, 'stephen', true];

    //Connecting with our interface
    this.user = {
      firstName: 'Jane',
      lastName: 'Doe',
      age: 30,
      address: {
        street: '150 Alamo Plaza',
        city: 'San Antonio',
        state: 'TX'
      }
    }


  }

  greeting() {
    //console.log('Hello there, ' + this.firstName + ' ' + this.lastName);
    return `Hello there, ${this.user.firstName} ${this.user.lastName}`
  }

  hasBirthday() {
    this.user.age = 30;
    return this.user.age += 1;
  }

}//End of Class - do not delete

//ALTERNATE WAY TO CREATE AN INTERFACE
// export interface User {
//   firstName: string,
//   lastName: string,
//   age: number,
//   address: {
//     street: string,
//     city: string,
//     state: string
//   }
// }

