import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from "rxjs";
import {Post} from "../models/Post";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type' : 'application/json'})
};

@Injectable({
  providedIn: 'root'
})

export class PostsService {
//Properties
  //set url as a property
  postsURL: string = 'https://jsonplaceholder.typicode.com/posts';

  //Inject the HTTPClient as a dependency
  constructor(private http: HttpClient) { }
  //Create a method, that will create a GET request
  getPost() : Observable<Post[]> {
    return this.http.get<Post[]>(this.postsURL)
  }

  //Create a method savePost()
  savePost(post: Post) : Observable<Post> {
    return this.http.post<Post>(this.postsURL, post, httpOptions)
  }

  updatePost(post: Post): Observable<Post> {
    const url = `${this.postsURL}/${post.id}`; //url = https://jsonplaceholder.typicode.com/post.id
    return this.http.put<Post>(url, post, httpOptions)
  }

  removePost(post:Post | number) : Observable<Post> {
    const id = typeof post === 'number' ? post : post.id;

    const url = `${this.postsURL}/${id}`;

    console.log('Deleting post...');
    alert('Post Removed');
    return this.http.delete<Post>(url, httpOptions);
  }

}
