// import { Component } from '@angular/core';
//
// @Component({
//   selector: 'app-root',
//   templateUrl: './app.component.html',
//   styleUrls: ['./app.component.css']
// })
// export class AppComponent {
//   title = 'Bravo Angular';
// }




// //Template Form

// import {Component} from "@angular/core";
// @Component({
//   selector: 'app-root',
//   template: '<form #corona="ngForm" (ngSubmit)="onclickSubmit(corona.value)">\n' +
//     '  <input type="text" name="patient_name" placeholder="name" ngModel>\n' +
//     '  <br/>\n' +
//     '  <input type="text" name="patient_age" placeholder="age" ngModel>\n' +
//     '  <br/>\n' +
//     '  <input type="submit" value="submit">\n' +
//     '\n' +
//     '</form>\n',
//   styleUrls: ['./app.component.css']
//
// })
// export class AppComponent {
//   onclickSubmit(formData) {
//     console.log('Corona infected patient is ' + formData.patient_name);
//     console.log('Corona infected patient age is ' + formData.patient_age);
//
//   }
// }


import {Component, OnInit} from "@angular/core";
import {FormGroup, FormControl} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  width: number = 100;
  height: number = 100;
  myStyle: Object = {
    'position': 'fixed',
    'width': '100%',
    'height': '100%',
    'z-index': -1,
    'top': 0,
    'left': 0,
    'right': 0,
    'bottom': 0,
    'background': 'rgba(175,26,26,0)'
  };
  myParams: object = {
    "particles": {
      "number": {
        "value": 80,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#2a4fe3" // this is the color of the particle points
      },
      "shape": {
        "type": "circle", // this refers to the shape of the point
        "polygon": {
          "nb_sides": 7
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 0.6,
        "random": false,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 3,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 40,
          "size_min": 0.1,
          "sync": false
        }
      },
      "line_linked": {
        "enable": true,
        "distance": 150,
        "color": "#5B566E", // this refers to the lines in the polygon
        "opacity": 0.4,
        "width": 1.5
      },
      "move": {
        "enable": true,
        "speed": 4,
        "direction": "none",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "bounce": false,
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 1200
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": false,
          "mode": "repulse"
        },
        "onclick": {
          "enable": false,
          "mode": "push"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 200,
          "duration": 0.4
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true
  };
  form: FormGroup;
  ngOnInit() {
    this.form = new FormGroup({
      patient_name: new FormControl(''),
      patient_age: new FormControl(''),
    });
  }
  onClickSubmit(formData) {
    console.log('Corona Effected patient name is: ' + formData.patient_name);
    console.log('Corona Effected patient age is: ' + formData.patient_age);
  }
}
