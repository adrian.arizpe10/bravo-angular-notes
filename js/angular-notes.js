//What is angular?

// a typescript-based open source framework (front and back end capability) (Typescript is a superset of JS)
//Created and maintained by Google
//First release was sept 14, 2016
//Current release 9.1.0

/*
Benefits of Using Angular

Efficient for Front end rapid development
Code organizations and productivity (singular components(nav-bar, blog-posts...)
Dynamic Content
Testing Capabilities built in


Angular !== Angular JS

Google.com (angular -js)

 */

//Commands:
// ng serve to start angular app
//cntrl + c - stop application

// runs on localhost:4200

//A component is a piece of the UI

//A component has a selector, template, style, and other properties

//Angular Goes: main.ts > app.module.ts > app.component > index.html > app.component.html

//Create components shorthand:
// ng generate component components/name-of-component
//ng g component components/name-of-component
//ng g c components/name-of-component

//Two ways to create an interface - inside component.ts under the class - create ts file
//BELOW IS INTERFACE EX
// export interface User {
//     firstName: string,
//         lastName: string,
//         age: number,
//         address: {
//         street: string,
//             city: string,
//             state: string
//     }
// }
//Angular MVC Design Pattern

//Component.HTML  represents the view in MVC
//Component.ts is the controller


//Template = HTML
//Styling = CSS
//Logic = TS
//Model = TS

// Reactive form - take in and send out data - robust / large amounts of data
//Template driven form - simple form

//Difference between constructor and NgOninit - constructor - used for dependency injections - ngoninit is for ajax calls or calls to the services


